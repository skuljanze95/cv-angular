import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";

@Injectable({
  providedIn: "root"
})
export class MobileMenuService {
  isActive: boolean;
  isActiveChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
    this.isActiveChange.subscribe(value => {
      this.isActive = value;
    });
  }

  togleisActive() {
    this.isActiveChange.next(!this.isActive);
  }
}
