import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from "@angular/fire/firestore";
import { observable, Observable } from "rxjs";
import { Portfolios } from "../models/item";

@Injectable({
  providedIn: "root",
})
export class PortfolioService {
  itemsCollection: AngularFirestoreCollection<Portfolios>;
  Items: Observable<Portfolios[]>;

  constructor(private afs: AngularFirestore) {}

  getItems() {
    this.Items = this.afs.collection("portfolios").valueChanges();
    return this.Items;
  }
}
