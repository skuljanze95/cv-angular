import { Component, OnInit } from "@angular/core";
import { MobileMenuService } from "../services/mobile-menu.service";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styleUrls: ["./about.component.scss"]
})
export class AboutComponent implements OnInit {
  isActive: boolean;
  constructor(private mobileMenu: MobileMenuService) {
    this.mobileMenu.isActiveChange.subscribe(value => {
      this.isActive = value;
    });
  }

  ngOnInit() {
    this.isActive = this.mobileMenu.isActive;
  }
}
