import { Component, OnInit } from "@angular/core";
import { PortfolioService } from "../services/portfolio.service";
import { Portfolios } from "../models/item";
import { Router } from "@angular/router";
import { MobileMenuService } from "../services/mobile-menu.service";
import { PortfolioDialogComponent } from "../portfolio-dialog/portfolio-dialog.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: "app-work",
  templateUrl: "./work.component.html",
  styleUrls: ["./work.component.scss"],
})
export class WorkComponent implements OnInit {
  portfolios: Portfolios[];
  isActive: boolean;
  constructor(
    private portfolioService: PortfolioService,
    private router: Router,
    private mobileMenu: MobileMenuService,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.mobileMenu.isActiveChange.subscribe((value) => {
      this.isActive = value;
    });
  }

  ngOnInit() {
    this.portfolioService.getItems().subscribe((items) => {
      this.portfolios = items;
      this.isActive = this.mobileMenu.isActive;
    });
  }

  openDialog(item): void {
    let dialogRef = this.dialog.open(PortfolioDialogComponent, {
      maxWidth: "100vw",
      width: "60%",
      data: item,
    });

    dialogRef.afterClosed().subscribe((result) => {});
  }

  openLink(String) {
    window.open(String, "_blank");
  }
}
