export interface Portfolios {
  Name?: string;
  Description?: string;
  Link?: string;
  GitHub?: string;
  Picture?: string;
  Video?: string;
  Technologies?: any;
  Pictures?: any;
}
