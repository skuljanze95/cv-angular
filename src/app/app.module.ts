import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { SkillsComponent } from "./skills/skills.component";
import { WorkComponent } from "./work/work.component";
import { ContactComponent } from "./contact/contact.component";
import { MoonComponent } from "./exercise-websites/moon/moon.component";
import { KawasakiComponent } from "./exercise-websites/kawasaki/kawasaki.component";

import { AngularFireModule } from "@angular/fire";
import { environment } from "../environments/environment";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { PortfolioService } from "./services/portfolio.service";
import { MatDialogModule } from "@angular/material/dialog";
import { PortfolioDialogComponent } from "./portfolio-dialog/portfolio-dialog.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AngularFullpageModule } from "@fullpage/angular-fullpage";

import { AngularFireAnalyticsModule } from "@angular/fire/analytics";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    SkillsComponent,
    WorkComponent,
    ContactComponent,
    PortfolioDialogComponent,
    KawasakiComponent,
    MoonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    MatDialogModule,
    BrowserAnimationsModule,
    AngularFullpageModule,

    AngularFireAnalyticsModule,
  ],
  entryComponents: [PortfolioDialogComponent],
  providers: [PortfolioService],
  bootstrap: [AppComponent],
})
export class AppModule {}
