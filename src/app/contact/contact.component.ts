import { Component, OnInit } from "@angular/core";
import { MobileMenuService } from "../services/mobile-menu.service";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ContactComponent implements OnInit {
  isActive: boolean;
  constructor(private mobileMenu: MobileMenuService) {
    this.mobileMenu.isActiveChange.subscribe(value => {
      this.isActive = value;
    });
  }

  ngOnInit() {
    this.isActive = this.mobileMenu.isActive;
  }
}
