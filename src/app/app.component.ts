import { Component } from "@angular/core";
import { MobileMenuService } from "../app/services/mobile-menu.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "anze-cv";
  isActive: boolean = false;

  constructor(private mobileMenu: MobileMenuService) {}
  onClick() {
    this.mobileMenu.isActiveChange.subscribe(value => {
      this.isActive = value;
    });

    this.mobileMenu.togleisActive();
  }
}
