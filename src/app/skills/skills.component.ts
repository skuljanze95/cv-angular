import { Component, OnInit } from "@angular/core";
import anime from "animejs/lib/anime.es";
import $ from "jquery";
import { MobileMenuService } from "../services/mobile-menu.service";

@Component({
  selector: "app-skills",
  templateUrl: "./skills.component.html",
  styleUrls: ["./skills.component.scss"]
})
export class SkillsComponent implements OnInit {
  isActive: boolean;
  constructor(private mobileMenu: MobileMenuService) {
    this.mobileMenu.isActiveChange.subscribe(value => {
      this.isActive = value;
    });
  }

  ngOnInit() {
    this.isActive = this.mobileMenu.isActive;
    var positionX = [
      168,
      50,
      -127,
      -94,
      -143,
      -59,
      186,
      -12,
      -8,
      60,
      28,
      -60,
      -25,
      78,
      69,
      55,
      165,
      146
    ];
    var positionY = [
      -86,
      85,
      -65,
      59,
      8,
      -124,
      -20,
      -66,
      -168,
      -12,
      -127,
      -23,
      25,
      -93,
      28,
      -59,
      38,
      -128
    ];
    $(".skill-tree >li").each(function(index) {
      anime({
        targets: this,
        translateX: positionX[index],
        translateY: positionY[index],
        color: "#FFF",
        opacity: "1",
        duration: 1800
      });
    });
  }
}
