import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-kawasaki",
  templateUrl: "./kawasaki.component.html",
  styleUrls: ["./kawasaki.component.scss"],
})
export class KawasakiComponent implements OnInit {
  config: any;
  fullpage_api: any;
  mobile;

  constructor() {
    // for more details on config options please visit fullPage.js docs
    this.config = {
      // fullpage options

      navigation: true,
      navigationPosition: "left",

      // fullpage callbacks
      afterResize: () => {
        console.log("After resize");
      },
      afterLoad: (origin, destination, direction) => {
        console.log(origin.index);
      },
    };
  }

  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
  }

  ngOnInit() {
    if (window.screen.width <= 600) {
      this.mobile = true;
    }
  }
}
