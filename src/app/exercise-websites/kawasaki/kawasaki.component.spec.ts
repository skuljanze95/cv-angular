import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KawasakiComponent } from './kawasaki.component';

describe('KawasakiComponent', () => {
  let component: KawasakiComponent;
  let fixture: ComponentFixture<KawasakiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KawasakiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KawasakiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
