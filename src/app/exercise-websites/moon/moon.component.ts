import { Component, OnInit } from "@angular/core";
import { gsap, Sine } from "gsap";
import CSSRulePlugin from "gsap/CSSRulePlugin";

gsap.registerPlugin(CSSRulePlugin);

@Component({
  selector: "app-moon",
  templateUrl: "./moon.component.html",
  styleUrls: ["./moon.component.scss"],
})
export class MoonComponent implements OnInit {
  fullpage_api: any;
  mobile;
  isActive: boolean = false;

  ngOnInit() {
    if (window.screen.width <= 600) {
      this.mobile = true;
    }
  }

  getRef(fullPageRef) {
    this.fullpage_api = fullPageRef;
    this.fullpage_api.setAllowScrolling(false);
  }

  config = {
    navigation: true,
    navigationPosition: "right",
    scrollingSpeed: 1,

    afterResize: () => {},
    afterLoad: (origin, destination, direction) => {
      window.onscroll = function () {
        window.scrollTo(0, 0);
      };
      if (destination.index == 0) {
        firstPageAnim();
      } else if (destination.index == 1) {
        secondPageAnim();
        this.fullpage_api.setAllowScrolling(false);
        setTimeout(() => {
          this.fullpage_api.setAllowScrolling(true);
        }, 2600);
      } else if (destination.index == 2) {
        thirdPageAnim();
        this.fullpage_api.setAllowScrolling(false);
        setTimeout(() => {
          this.fullpage_api.setAllowScrolling(true);
        }, 2600);
      } else if (destination.index == 3) {
        forthPageAnim();
        this.fullpage_api.setAllowScrolling(false);
        setTimeout(() => {
          this.fullpage_api.setAllowScrolling(true);
        }, 2600);
      }

      function firstPageAnim() {
        if (origin.index != 0) {
          gsap.to(
            ".hidetext-top-s1",

            { y: 0, duration: 1.3, ease: Sine.easeOut, delay: 2.6 }
          );
          gsap.to(
            ".hidetext-left-s1",

            { x: 0, duration: 1.3, ease: Sine.easeOut, delay: 2.6 }
          );
          gsap.to(".moon", {
            width: "827px",
            opacity: 1,
            duration: 1.5,
            ease: Sine.easeOut,
            x: 0,
            y: 0,
            delay: 1.8,
          });
          gsap.to(".astronaut", {
            width: "658px",
            opacity: 1,
            duration: 1.5,
            ease: Sine.easeOut,
            x: 0,
            y: 0,
            delay: 1.8,
          });
          gsap.to(".moon1", {
            //width: "883px",

            duration: 1.5,
            delay: 1.8,

            top: "150%",
            ease: Sine.easeOut,
          });
        }
      }
      function secondPageAnim() {
        gsap.to(
          ".hidetext-top-s2-1",

          { y: 0, duration: 1.3, ease: Sine.easeOut, delay: 2.6 }
        );
        gsap.to(
          ".about",

          { opacity: 1, duration: 0.9, ease: Sine.easeOut, delay: 3.6 }
        );
        gsap.to(
          ".lianim",

          { x: 0, duration: 0.9, ease: Sine.easeOut, delay: 3.6 }
        );
        gsap.to(".moon", {
          width: "427px",
          opacity: 1,
          duration: 1.5,
          ease: Sine.easeOut,
          x: 0,
          y: 0,
          delay: 1.3,
        });
        gsap.to(".astronaut", {
          width: "508px",
          opacity: 1,
          duration: 1.5,
          ease: Sine.easeOut,
          x: -600,
          y: -120,
          delay: 1.3,
        });
        gsap.to(".moon1", {
          //width: "883px",

          duration: 1.5,
          delay: -1.5,

          top: "150%",
          ease: Sine.easeOut,
        });
      }
      function thirdPageAnim() {
        gsap.to(".projects", {
          opacity: 1,
          duration: 0.9,
          delay: 2.7,

          ease: Sine.easeOut,
        });
        gsap.to(".hidetext-top-s3", {
          duration: 1.3,
          delay: 2.3,
          y: 0,
          ease: Sine.easeOut,
        });

        gsap.to(".moon", {
          width: "110px",
          opacity: 1,
          duration: 1.5,
          y: -200,
          x: 200,
          delay: 0.9,
          ease: Sine.easeOut,
        });
        gsap.to(".astronaut", {
          width: "508px",
          opacity: 1,
          duration: 1.5,
          delay: 0.9,
          x: -300,
          y: -680,
          ease: Sine.easeOut,
        });
        gsap.to(".moon1", {
          //width: "883px",

          duration: 1.5,
          delay: 0.9,

          top: "150%",
          ease: Sine.easeOut,
        });
      }

      function forthPageAnim() {
        gsap.to(".moon", {
          width: "110px",
          opacity: 1,
          duration: 1.5,
          y: -200,
          x: 200,
          delay: 0.9,
          ease: Sine.easeOut,
        });
        gsap.to(".astronaut", {
          width: "508px",
          opacity: 1,
          duration: 1.5,
          delay: 0.9,

          x: -400,
          y: 110,
          ease: Sine.easeOut,
        });
        gsap.to(".moon1", {
          duration: 1.5,
          delay: 0.9,

          top: "110%",
          ease: Sine.easeOut,
        });
        gsap.to(".astronaut", {
          width: "608px",
          opacity: 1,
          duration: 1.5,
          delay: 2.5,

          x: -450,
          y: -50,
          ease: Sine.easeOut,
        });
        gsap.to(".hidetext-top-s4", {
          opacity: 1,
          duration: 1.3,
          delay: 2.5,
          y: 0,
          ease: Sine.easeOut,
        });

        gsap.to(".hidetext-bottom-s4", {
          opacity: 1,
          duration: 0.9,
          delay: 3.1,

          ease: Sine.easeOut,
        });
      }
    },
    onLeave: function (origin, destination, direction) {
      if (origin.index == 0) {
        gsap.to(".hidetext-top-s1", { y: -240, duration: 1.3 });
        gsap.to(".hidetext-left-s1", { x: "-100%", duration: 1.3 });
      }
      if (origin.index == 1) {
        gsap.to(".hidetext-top-s2-1", {
          y: -200,
          duration: 0.9,
          ease: Sine.easeOut,
        });
        gsap.to(".about", {
          opacity: 0,
          duration: 0.9,
          ease: Sine.easeOut,
          delay: 0.3,
        });
        gsap.to(".lianim", {
          x: -220,
          duration: 0.9,
          ease: Sine.easeOut,
          delay: 0.8,
        });
      }
      if (origin.index == 2) {
        gsap.to(".projects", {
          opacity: 0,
          duration: 0.9,

          ease: Sine.easeOut,
        });
        gsap.to(".hidetext-top-s3", {
          duration: 1.3,

          y: -200,
          ease: Sine.easeOut,
        });
      }
      if (origin.index == 3) {
        gsap.to(".hidetext-top-s4", {
          opacity: 1,
          duration: 0.9,

          y: -360,
          ease: Sine.easeOut,
        });

        gsap.to(".hidetext-bottom-s4", {
          opacity: 0,
          duration: 0.9,
          delay: 0.3,

          ease: Sine.easeOut,
        });
      }
    },
  };

  constructor() {
    // for more details on config options please visit fullPage.js docs
  }

  onClick() {
    this.isActive = !this.isActive;
  }

  async firstAnim() {
    setTimeout(() => {
      this.fullpage_api.setAllowScrolling(true);
    }, 4200);
    gsap.to(".overlay", {
      top: "-100vh",
      duration: 2,
      ease: Sine.easeIn,
    });

    gsap.to(".hidetext-top-s1", { y: 0, duration: 1.3, delay: 4 });
    gsap.to(".hidetext-left-s1", { x: 0, duration: 1.3, delay: 4 });

    gsap.to(".moon", {
      width: "827px",
      opacity: 1,
      duration: 1.8,
      delay: 2.5,
      ease: Sine.easeOut,
    });
    gsap.to(".astronaut", {
      width: "658px",
      opacity: 1,
      duration: 1.8,
      delay: 2.5,
      ease: Sine.easeOut,
    });
    gsap.from(".social-item", {
      y: -10,
      opacity: 0,
      duration: 1.3,
      delay: 4,
      ease: Sine.easeOut,
      stagger: 0.1,
    });
    gsap.from(".header", {
      y: -10,
      opacity: 0,

      duration: 1.3,
      delay: 4,
      ease: Sine.easeOut,
    });
    gsap.from(".follow span", {
      width: "60px",
      duration: 1,
      delay: 4,
      ease: Sine.easeOut,
    });
  }
}
