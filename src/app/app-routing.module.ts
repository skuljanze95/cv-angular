import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { SkillsComponent } from "./skills/skills.component";
import { WorkComponent } from "./work/work.component";
import { ContactComponent } from "./contact/contact.component";

import { KawasakiComponent } from "./exercise-websites/kawasaki/kawasaki.component";
import { MoonComponent } from "./exercise-websites/moon/moon.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "skills", component: SkillsComponent },
  { path: "work", component: WorkComponent },
  { path: "contact", component: ContactComponent },
  { path: "work/kawasaki", component: KawasakiComponent },
  { path: "work/moon", component: MoonComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
