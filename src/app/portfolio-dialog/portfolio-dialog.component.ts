import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-portfolio-dialog",
  templateUrl: "./portfolio-dialog.component.html",
  styleUrls: ["./portfolio-dialog.component.scss"],
})
export class PortfolioDialogComponent implements OnInit {
  options = { noWrap: false, dist: -100 };
  constructor(
    public dialogRef: MatDialogRef<PortfolioDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  openLink(String) {
    window.open(String, "_blank");
  }
}
